# **Zabbix de 0 a 100 v2 - 2022**

## Estructura de ficheros
```
├── README.txt                       Esta documentacion.
├── infra                            Oracle Linux 8.
|   ├── provisioning.sh              Aprovisionamiento de la maquinas Cliente y Proxy.
|   ├── provisioning-dockers.sh      Aprovisionamiento de la maquina Zabbix Server + Dockers para ejemplos de curso.
|   ├── ResponseTimeCURL.sh          Script que utilizaremos en algun ejemplo.
|   ├── update-motd.sh               Muestra una pantalla de inicio personalizada.
|   ├── Vagrantfile                  Despliegue de la Infra con Vagrant + VirtualBox.
|   └── Vagrant-Help.txt             Ejemplos utiles de comandos de Vagrant.
├───scripts                          External scripts
└───templates                        Templates de ejemplo

```
# Requerimientos minimos
**- El entorno se desplegara con maquinas virtuales gestionadas con VirtualBox y Vagrant**
```
 - VirtualBox Versión 6.1.30     Download: https://www.virtualbox.org/
 - Vagrant Versión 2.2.19        Download: https://www.vagrantup.com/downloads
 - Git                           For Windows: https://git-scm.com/download/win
```

# Despliegue de la Infraestructura
## En Linux desde un BASH

```
# cd
# git clone https://mobarrio@bitbucket.org/mobarrio/zabbix-de-0-a-100-v2.git
# cd zabbix-de-0-a-100-v2
# cd infra
# vagrant up

... Esperamos a que despliegue las maquinas

# vagrant ssh zbxsrv01         NOTA: La password del usuario de root es vagrant
```
## En Windows desde un CMD

```
C:\> cd \
C:\> git clone https://mobarrio@bitbucket.org/mobarrio/zabbix-de-0-a-100-v2.git
C:\> cd zabbix-de-0-a-100-v2
C:\zabbix-de-0-a-100-v2> cd infra
C:\zabbix-de-0-a-100-v2\infra> vagrant up

... Esperamos a que despliegue las maquinas

C:\zabbix-de-0-a-100-v2\infra> vagrant ssh zbxsrv01         NOTA: La password del usuario de root es vagrant
```

