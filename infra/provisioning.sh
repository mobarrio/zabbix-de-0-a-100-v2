#!/usr/bin/bash

# Habilita SSH remoto
echo "[TAREA 1] Habilitar la autenticacion via SSH"
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
sed -i 's/#PermitRootLogin yes/PermitRootLogin yes/g' /etc/ssh/sshd_config
sed -i 's/PrintMotd yes/PrintMotd no/g' /etc/ssh/sshd_config
sed -i 's/PrintLastLog yes/PrintLastLog no/g' /etc/ssh/sshd_config
systemctl restart sshd

# Set Root password
echo "[TAREA 2] Configurar la password de root: zabbix"
echo -e "zabbix\nzabbix" | passwd root >/dev/null 2>&1

# Desactiva SELinux
echo "[TAREA 3] Parando y deshabilitando el firewall"
systemctl stop firewalld && systemctl disable firewalld

# Desactiva SELinux
echo "[TAREA 4] Desactiva SELinux"
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
setenforce 0

# Limpia el motd
echo "[TAREA 5] Limpiando MOTD"
>/etc/motd 

# Actualiza e instala paqueteria adisional
echo "[TAREA 6] Instalando software adisional"
dnf update -y
dnf install -y oracle-epel-release-el8
dnf config-manager --set-enabled ol8_developer_EPEL
dnf clean all
dnf install -y git jq curl wget net-tools dnf-utils sysstat tree stress redhat-lsb* lsof dnf-utils zip unzip dos2unix

# Configurando TimeZone Europe/Madrid
echo "[TAREA 7] Configurando TimeZone Europe/Madrid"
timedatectl set-timezone Europe/Madrid

# Actualizando el /etc/hosts
echo "[TAREA 8] Actualizando el /etc/hosts"
cat >>/etc/hosts<<EOF
10.1.100.100   zbxserver01
10.1.100.101   zbxclient01
10.1.100.102   zbxproxy01
EOF

# Personalizacion del entorno
echo "[TAREA 9] Personalizacion del entorno"
cp /vagrant/update-motd.sh /etc/update-motd
chmod 755 /etc/update-motd
dos2unix /etc/update-motd
grep -qF 'update-motd' /etc/profile || echo "[ -x /etc/update-motd ] && /etc/update-motd" >>/etc/profile
grep -qxF "alias l='ls -la'" ~/.bashrc || echo "alias l='ls -la'" >>~/.bashrc
grep -qxF 'unalias ls' ~/.bashrc || echo "unalias ls" >>~/.bashrc

#Fresh Reboot
echo "[TAREA 10] Reboot del sistema"
reboot
